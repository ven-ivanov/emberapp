import DS from 'ember-data';

export default DS.RESTAdapter.extend({
    needs: ['application'],
    currentUser: Ember.computed.alias('controllers.application.currentUser'),
    namespace: '',
    host: 'http://localhost:3000',
    headers: function() {
        return {
            token: this.get('currentUser').token
        };
    }
});
