import Ember from 'ember';

export default Ember.Controller.extend({
    needs: ['application'],
    currentUser: Ember.computed.alias('controllers.application.currentUser'),
    isLoggedIn: Ember.computed.alias('controllers.application.isLoggedIn'),
    actions: {
        save: function() {
            var title = this.get('title');
            var html= this.get('html');
            var currentUser = this.get('currentUser');
            if(currentUser){
                var date = new Date();
                if(!title.trim()) {return; } //empty string

                var post = this.store.createRecord('blogpost', {
                    title:title,
                    html:html,
                    user_id: currentUser.id
                });
                this.set('title','');
                this.set('html','');
                //this.set('author','');
                post.save();
            }

            this.transitionToRoute('blogposts');

        }
    }
});