import Ember from 'ember';

export default Ember.Controller.extend({
    needs: ['application'],
    currentUser: Ember.computed.alias('controllers.application.currentUser'),
    isLoggedIn: Ember.computed.alias('controllers.application.isLoggedIn'),
    actions: {
        login: function() {
            var email= this.get('email');
            var password = this.get('password');

            //do login
            var that = this;
            Ember.$.post("http://localhost:3000/users/login",{
                email: email,
                password: password
            }, function(data) {
                //manipulate labels
                if(data.token !== null){
                    //loggined correctly
                    that.set('currentUser', data);
                    that.set('isLoggedIn', true);
                }else{
                    that.set('currentUser',null);
                }
            });
            this.transitionToRoute('blogposts');
        }
    }
});
