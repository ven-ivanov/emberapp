import Ember from 'ember';

export default Ember.Controller.extend({
    needs: ['application'],
    currentUser: Ember.computed.alias('controllers.application.currentUser'),
    isLoggedIn: Ember.computed.alias('controllers.application.isLoggedIn'),
    actions: {
        logout: function() {
            var email= this.get('email');
            var password = this.get('password');

            //do login
            var that = this;
            Ember.$.post("http://localhost:3000/users/logout",{
                email: email,
                password: password
            }, function(data) {
                //manipulate labels
                if(data.message !== null){
                    //loggined correctly
                    that.set('currentUser', null);
                    that.set('isLoggedIn', false);
                }else{
                    that.set('currentUser',null);
                }
            });
            this.transitionToRoute('index');
        }
    }
});
