import Ember from 'ember';

export default Ember.Controller.extend({
    actions: {
        save: function() {
            var email = this.get('email');
            var name= this.get('name');
            var password = this.get('password');

            var user = this.store.createRecord('user', {
                email:  email,
                name:  name,
                password:  password
            });
            this.set('email','');
            this.set('name','');
            this.set('password','');
            user.save();
            this.transitionToRoute('users');

        }
    }
});
