import DS from 'ember-data';

export default DS.Model.extend({
    title: DS.attr('string'),
    html: DS.attr('string'),
    user_id: DS.attr(),
    created_at: DS.attr('date'),
    updated_at: DS.attr('date')
});
