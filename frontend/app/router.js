import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.resource("blogposts", function() {
      this.route("new");
      this.route("edit",{
          path: "/edit/:blogpost_id"
      });
  });
    this.resource("users", function() {
        this.route("new");
        this.route("edit",{
            path: "/edit/:user_id"
        });
        this.route("login");
        this.route("logout");
    });
});

export default Router;
