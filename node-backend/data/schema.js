"use strict";

var Schema = {
	users: {
		id: {type: "increments", nullable: false, primary: true},
		email: {type: "string", maxlength: 254, nullable: false, unique: true},
		name: {type: "string", maxlength: 150, nullable: false},
		password: {type: "string", nullable: false},
		token: {type: "string", nullable: true}
	},
	// html: {type: "text", fieldtype: "medium", nullable: false},
	blogposts: {
		id: {type: "increments", nullable: false, primary: true},
		user_id: {type: "integer", nullable: false, unsigned: true},
		title: {type: "string", maxlength: 150, nullable: false},
		html: {type: "string", maxlength: 150, nullable: false},
		created_at: {type: "dateTime", nullable: false},
		updated_at: {type: "dateTime", nullable: true}
	}
};

module.exports = Schema;