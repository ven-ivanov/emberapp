"use strict";

var BlogpostController = {},
	Moment = require("moment"),
	Collections = require("../data/collection.js");

BlogpostController.getAll = function (req, res) {
	Collections.BlogpostCollection.forge()
	.fetch()
	.then(function (result) {
		res.status(200).json({blogposts: result});
	})
	.catch(function (err) {
		res.status(500).json(err);
	});
};

BlogpostController.getPost = function (req, res) {
	getBlogpostWithId(req.params.id)
	.then(function (post) {
		if (!post) {
			res.status(404).json({message: "blogpost not found"});
		} else {
			res.status(200).json(post);
		}
	})
	.catch(function (err) {
		res.status(500).json({message: err.message});
	});
};

BlogpostController.create = function (req, res) {
	// Save post
    var payLoad = req.body.blogpost;
	Collections.BlogpostCollection.forge()
	.create({
	/*	user_id: req.body.user_id,*/
        user_id: payLoad.user_id,
		title: payLoad.title,
		html: payLoad.html,
		created_at: Moment().format()
	})
	.then(function (blogpost) {
    	res.status(200).json(blogpost);
	})
	.catch(function (err) {
		res.status(500).json({message: err.message});
	});
};

BlogpostController.update = function (req, res) {
    var payLoad = req.body.blogpost;
	getBlogpostWithId(req.params.id)
	.then(function (blogpost) {
		if (!blogpost) {
			res.status(404).json({message: "blogpost not found"});
		} else {
			blogpost.save({
				user_id: payLoad.user_id || blogpost.get("id"),
				title: payLoad.title || blogpost.get("title"),
				html: payLoad.html || blogpost.get("html"),
				updated_at: Moment().format()
			})
			.then(function (result) {
                res.status(200).json("blogpost updated");
			})
			.catch(function (err) {
				res.status(500).json({message: err.message});
			});
		}
	})
	.catch(function (err) {
		res.status(500).json({message: err.message});
	})
};

BlogpostController.destroy = function (req, res) {
	getBlogpostWithId(req.params.id)
	.then(function (blogpost) {
		if (!blogpost) {
			res.status(404).json({error:"Blogpost not found", message: err.message});
		} else {

			// Then delete the model itself
			blogpost.destroy()
			.then(function () {
				res.status(200).json({});
			})
			.catch(function (err) {
				res.status(500).json({error:"Blogpost delete failed", message: err.message})
			});
		}
	})
	.catch(function (err) {
		res.status(500).json({message: err.message})
	});
};

var getBlogpostWithId = function (id) {

	return Collections.BlogpostCollection.forge()
	.query(function (qb) {
		qb.where("id", "=", id);
	})
        .fetchOne();
}

module.exports = BlogpostController;














