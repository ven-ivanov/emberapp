"use strict";

var Express = require("express"),
    App = Express(),
    Http = require("http"),
    BodyParser = require("body-parser"),
    Router = Express.Router(),
    MethodOverride = require("method-override"),
    Multer = require("multer"),
    Db = require("./database.js"),
    AuthController = require("./routes/auth.js"),
    UserController = require("./routes/user.js"),
    BlogpostController = require("./routes/blogpost.js");

App.use(Multer());
App.use(MethodOverride());
App.use(BodyParser.json());
App.use(BodyParser.urlencoded({extended: true}));
App.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    next();
});

Db.initialisation();

Http.createServer(App).listen(3000);

App.get("/users", UserController.getAll);
App.post("/users", UserController.create);
App.post("/users/login", UserController.login);

App.get("/users/:id", UserController.getUser);
App.post("/users/logout", UserController.logout);
App.put("/users/:id", UserController.update);
App.delete("/users/:id",UserController.destroy);

/*App.get("/users/:id", AuthController.requireUser(), UserController.getUser);
App.post("/users/logout", AuthController.requireUser(), UserController.logout);
App.put("/users/:id", AuthController.requireUser(), UserController.update);
App.delete("/users/:id",AuthController.requireUser(), UserController.destroy);*/


App.get("/blogposts", BlogpostController.getAll);
App.get("/blogposts/:id", BlogpostController.getPost);
App.post("/blogposts", BlogpostController.create);
App.put("/blogposts/:id", BlogpostController.update);
App.delete("/blogposts/:id", BlogpostController.destroy);

/*App.get("/blogposts/:id", AuthController.requireUser(), BlogpostController.getPost);
App.post("/blogposts", AuthController.requireUser(), BlogpostController.create);
App.put("/blogposts/:id", AuthController.requireUser(), BlogpostController.update);
App.delete("/blogposts/:id", AuthController.requireUser(), BlogpostController.destroy);*/

// App.post("/tag", TagController.create);